#ifndef NETWORKTHREAD_HPP
#define NETWORKTHREAD_HPP

#include <QLabel>
#include <QObject>
#include <QThread>

class MainWindow;

class NetworkThread : public QThread {
		Q_OBJECT
	    public:
		NetworkThread(MainWindow* realParent, QLabel* lblError, QObject* parent = nullptr);

		void run() override;

		QString valueToSend;
		bool spinned = false;

	    signals:
		void successfulLogin();
		void successfulResult(QString res);
		void error(int socketError, const QString& message);

	    private:
		MainWindow* parentt;
		QString hostname;
		quint16 port;
		bool quit;
		QLabel* lblErr;
};
#endif // NETWORKTHREAD_HPP
