#include "MainWindow.hpp"
#include <QDebug>
#include <QPainter>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent) { }

MainWindow::~MainWindow() {
		exit(0);
}

void MainWindow::startGame() {
		scene = new QGraphicsScene(this);
		scene->setSceneRect(0, 0, resolutionWidth, resolutionHeight);
		background_image = new QGraphicsPixmapItem(QPixmap(":/resources/background.png").scaled(resolutionWidth, resolutionHeight));
		background_image->setPos(0, 0);
		scene->addItem(background_image);

		view = new QGraphicsView(scene);
		view->setGeometry(0, 0, resolutionWidth, resolutionHeight);

		view->setMaximumSize(resolutionWidth, resolutionHeight);
		view->setMinimumSize(resolutionWidth, resolutionHeight);
		view->centerOn(resolutionWidth / 2, resolutionHeight / 2);

		view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		view->setWindowState(Qt::WindowFullScreen);
		view->show();

		loginScreen();
}

void MainWindow::loginScreen() {
		{
				delete title_image;
		}
		title_image = new QGraphicsPixmapItem(QPixmap(":/resources/title.png").scaled(resolutionWidth, resolutionHeight));
		title_image->setPos(-32, 0);
		scene->addItem(title_image);

		exitButton = new QPushButton(view);
		exitButton->setGeometry(resolutionWidth - 36, 4, 32, 32);
		exitButton->setText("X");
		connect(exitButton, &QPushButton::clicked, this, &MainWindow::exitNow);
		exitButton->show();

		lePassword = new QLineEdit(view);
		lePassword->setGeometry(resolutionWidth / 2 - 64, resolutionHeight / 2, 128, 32);
		lePassword->show();

		leUsername = new QLineEdit(view);
		leUsername->setGeometry(resolutionWidth / 2 - 64, resolutionHeight / 2 - 64, 128, 32);
		leUsername->show();

		loginButton = new QPushButton(view);
		loginButton->setGeometry(resolutionWidth / 2 - 66, resolutionHeight / 2 + 64, 64, 32);
		loginButton->setText("Login");
		loginButton->show();

		registerButton = new QPushButton(view);
		registerButton->setGeometry(resolutionWidth / 2 + 2, resolutionHeight / 2 + 64, 64, 32);
		registerButton->setText("Register");
		registerButton->show();

		lblUsername = new QLabel(view);
		lblUsername->setGeometry(resolutionWidth / 2 - 150, resolutionHeight / 2 - 64, 96, 32);
		lblUsername->setText("Username:");
		lblUsername->show();

		lblPassword = new QLabel(view);
		lblPassword->setGeometry(resolutionWidth / 2 - 150, resolutionHeight / 2, 96, 32);
		lblPassword->setText(" Password:");
		lblPassword->show();

		lblError = new QLabel(view);
		lblError->setGeometry(resolutionWidth / 2 - 64, resolutionHeight / 2 + 128, 196, 32);
		lblError->show();
		lblError->setText("Welcome.");

		connect(loginButton, &QPushButton::clicked, this, &MainWindow::on_pbLogin_clicked);
		connect(registerButton, &QPushButton::clicked, this, &MainWindow::on_pbRegister_clicked);
}

void MainWindow::game() {
		delete lblError;
		delete lblPassword;
		delete lblUsername;
		delete leUsername;
		delete lePassword;
		delete loginButton;
		delete registerButton;
		scene->removeItem(title_image);

		table_image = new QGraphicsPixmapItem(QPixmap(":/resources/table.png").scaled(resolutionWidth, resolutionHeight));
		table_image->setPos(0, 0);
		scene->addItem(table_image);
		scene->addItem(title_image);

		spin_image = new QGraphicsPixmapItem(QPixmap(":/resources/spin button.png").scaled(resolutionWidth, resolutionHeight));
		spin_image->setPos(0, 0);
		scene->addItem(spin_image);

		spinButton = new QPushButton(view);
		spinButton->setGeometry(resolutionWidth / 2 - 64, resolutionHeight - 64, 128, 48);
		spinButton->setFlat(true);
		spinButton->setStyleSheet("QPushButton { background-color: transparent; border: 0px }");
		spinButton->show();

		connect(spinButton, &QPushButton::clicked, this, &MainWindow::on_pbSpin_clicked);

		pbReel1 = new QPushButton(view);
		pbReel1->setGeometry(1 * resolutionWidth / 7.5, resolutionHeight / 2 - 256, 256, 560);
		pbReel1->setFlat(true);
		pbReel1->setStyleSheet("QPushButton { background-color: transparent; border: 0px }");
		QPixmap reel1Pixmap = reel1.copy(QRect(0, reel1Y, 256, 400));
		pbReel1->setIcon(reel1Pixmap);
		pbReel1->setIconSize(QSize(256, 400));
		pbReel1->show();

		pbReel2 = new QPushButton(view);
		pbReel2->setGeometry(2 * resolutionWidth / 7.5, resolutionHeight / 2 - 256, 256, 560);
		pbReel2->setFlat(true);
		pbReel2->setStyleSheet("QPushButton { background-color: transparent; border: 0px }");
		QPixmap reel2Pixmap = reel1.copy(QRect(0, reel2Y, 256, 400));
		pbReel2->setIcon(reel2Pixmap);
		pbReel2->setIconSize(QSize(256, 400));
		pbReel2->show();

		pbReel3 = new QPushButton(view);
		pbReel3->setGeometry(3 * resolutionWidth / 7.5, resolutionHeight / 2 - 256, 256, 560);
		pbReel3->setFlat(true);
		pbReel3->setStyleSheet("QPushButton { background-color: transparent; border: 0px }");
		QPixmap reel3Pixmap = reel1.copy(QRect(0, reel3Y, 256, 400));
		pbReel3->setIcon(reel3Pixmap);
		pbReel3->setIconSize(QSize(256, 400));
		pbReel3->show();

		pbReel4 = new QPushButton(view);
		pbReel4->setGeometry(4 * resolutionWidth / 7.5, resolutionHeight / 2 - 256, 256, 560);
		pbReel4->setFlat(true);
		pbReel4->setStyleSheet("QPushButton { background-color: transparent; border: 0px }");
		QPixmap reel4Pixmap = reel1.copy(QRect(0, reel4Y, 256, 400));
		pbReel4->setIcon(reel4Pixmap);
		pbReel4->setIconSize(QSize(256, 400));
		pbReel4->show();

		pbReel5 = new QPushButton(view);
		pbReel5->setGeometry(5 * resolutionWidth / 7.5, resolutionHeight / 2 - 256, 256, 560);
		pbReel5->setFlat(true);
		pbReel5->setStyleSheet("QPushButton { background-color: transparent; border: 0px }");
		QPixmap reel5Pixmap = reel1.copy(QRect(0, reel5Y, 256, 400));
		pbReel5->setIcon(reel5Pixmap);
		pbReel5->setIconSize(QSize(256, 400));
		pbReel5->show();

		leBet = new QLineEdit(view);
		leBet->setText("Input bet");
		leBet->setGeometry(resolutionWidth / 3.5, resolutionHeight - 128, 128, 32);
		leBet->show();

		lblCredits = new QLabel(view);
		lblCredits->setGeometry(3 * resolutionWidth / 5, resolutionHeight - 128, 256, 32);
		lblCredits->setText("Credits: " + QString::number(totalCreds));
		lblCredits->show();
}

void MainWindow::on_pbRegister_clicked() {
		thread = new NetworkThread(this, lblError, this);
		thread->valueToSend = "register###" + leUsername->text() + "###" + lePassword->text();
		connect(thread, &NetworkThread::successfulLogin, this, &MainWindow::successfulLogin);
		connect(thread, &NetworkThread::finished, thread, &NetworkThread::deleteLater);
		thread->start();
}

void MainWindow::on_pbLogin_clicked() {
		thread = new NetworkThread(this, lblError, this);
		thread->valueToSend = "login###" + leUsername->text() + "###" + lePassword->text();
		connect(thread, &NetworkThread::successfulLogin, this, &MainWindow::successfulLogin);
		connect(thread, &NetworkThread::finished, thread, &NetworkThread::deleteLater);
		thread->start();
}

void MainWindow::on_pbSpin_clicked() {
		if ( leBet->text().length() == 0 || leBet->text() == "Input bet" ) {
				return;
		}
		if ( !timer.isActive() ) {
				connect(&this->timer, &QTimer::timeout, this, &MainWindow::timerTick);
				timer.start(16);
				thread->valueToSend = "spin###" + leBet->text();
				condition.wakeOne();
		}
}

void MainWindow::timerTick() {
		timeElapsed += 16;
		if ( timeElapsed >= 1000 ) {
				timeElapsed = 0;
				secondsElapsed++;
		}

		reel1Y += 40;
		if ( reel1Y >= 2048 - 500 ) {
				reel1Y = 128;
		}
		QPixmap reel1Pixmap = reel1.copy(QRect(0, reel1Y, 256, 400));
		pbReel1->setIcon(reel1Pixmap);

		reel2Y += 42;
		if ( reel2Y >= 2048 - 500 ) {
				reel2Y = 256;
		}
		QPixmap reel2Pixmap = reel1.copy(QRect(0, reel2Y, 256, 400));
		pbReel2->setIcon(reel2Pixmap);

		reel3Y += 41;
		if ( reel3Y >= 2048 - 500 ) {
				reel3Y = 512;
		}
		QPixmap reel3Pixmap = reel1.copy(QRect(0, reel3Y, 256, 400));
		pbReel3->setIcon(reel3Pixmap);

		reel4Y += 43;
		if ( reel4Y >= 2048 - 500 ) {
				reel4Y = 756;
		}
		QPixmap reel4Pixmap = reel1.copy(QRect(0, reel4Y, 256, 400));
		pbReel4->setIcon(reel4Pixmap);

		reel5Y += 44;
		if ( reel5Y >= 2048 - 500 ) {
				reel5Y = 756;
		}
		QPixmap reel5Pixmap = reel1.copy(QRect(0, reel5Y, 256, 400));
		pbReel5->setIcon(reel5Pixmap);

		if ( secondsElapsed >= 3 && success ) {
				secondsElapsed = 0;
				timeElapsed = 0;
				disconnect(&this->timer, &QTimer::timeout, this, &MainWindow::timerTick);
				timer.stop();
				success = false;
				drawAllFruit();
		}
}

void MainWindow::successfulLogin() {
		game();
}

void MainWindow::successfulResult(QString res) {
		vockice_mi_zivot_upropastile.clear();
		if ( res.length() != 0 ) {
				QStringList parts = res.split(QString("-"));
				QStringList firstPart = parts.at(0).split("#");
				int rowsNum = firstPart.at(0).toInt();
				int collumns = firstPart.at(1).toInt();
				int newCredits = firstPart.at(2).toUInt();
				if ( newCredits > totalCreds ) {
						lblCredits->setText("Credits: " + firstPart.at(2).trimmed() + "(+" + QString::number(newCredits - totalCreds)
								    + "!)");
				} else {
						lblCredits->setText("Credits: " + firstPart.at(2).trimmed());
				}
				totalCreds = newCredits;

				QStringList rows = parts.at(1).split("/");
				QVector<QString> row;
				for ( int j = 0; j < rowsNum; j++ ) {
						QStringList rowSplit = rows.at(j).split("#");
						for ( int i = 0; i < collumns; i++ ) {
								row.append(rowSplit.at(i).trimmed());
						}
						vockice_mi_zivot_upropastile.append(row);
						row.clear();
				}
		}

		success = true;
}

void MainWindow::exitNow() {
		exit(0);
}

void MainWindow::drawAllFruit() {

		for ( int i = 0; i < 5; i++ ) {
				QPixmap reelPixmap(256, 450);
				reelPixmap.fill(Qt::transparent);
				{
						QPainter painter;
						painter.begin(&reelPixmap);
						QPixmap top = returnIcon(vockice_mi_zivot_upropastile[0][i]);
						painter.drawPixmap(64, 0, top);

						QPixmap mid = returnIcon(vockice_mi_zivot_upropastile[1][i]);
						painter.drawPixmap(64, 128, mid);

						QPixmap bot = returnIcon(vockice_mi_zivot_upropastile[2][i]);
						painter.drawPixmap(64, 256, bot);
						painter.end();
				}

				if ( i == 0 ) {
						pbReel1->setIcon(reelPixmap);
				} else if ( i == 1 ) {
						//QTimer::singleShot(1000, [this, reelPixmap]() { this->pbReel2->setIcon(reelPixmap); });
						pbReel2->setIcon(reelPixmap);
				} else if ( i == 2 ) {
						pbReel3->setIcon(reelPixmap);
				} else if ( i == 3 ) {
						pbReel4->setIcon(reelPixmap);
				} else if ( i == 4 ) {
						pbReel5->setIcon(reelPixmap);
				}
		}
}

QPixmap MainWindow::returnIcon(QString name) {
		if ( name == "banana" ) {
				return QPixmap(":/symbols/resources/symbols/banana.png");
		}
		if ( name == "coconut" ) {
				return QPixmap(":/symbols/resources/symbols/coconut.png");
		}
		if ( name == "wildcard" ) {
				return QPixmap(":/symbols/resources/symbols/jokerWild.png");
		}
		if ( name == "K" ) {
				return QPixmap(":/symbols/resources/symbols/K.png");
		}
		if ( name == "mushroom" ) {
				return QPixmap(":/symbols/resources/symbols/mushroom.png");
		}
		if ( name == "pineapple" ) {
				return QPixmap(":/symbols/resources/symbols/pineapple.png");
		}
		if ( name == "peach" ) {
				return QPixmap(":/symbols/resources/symbols/peach.png");
		}
}
