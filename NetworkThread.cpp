#include "NetworkThread.hpp"
#include "MainWindow.hpp"
#include <QDataStream>
#include <QFile>
#include <QTcpSocket>

NetworkThread::NetworkThread(MainWindow* realParent, QLabel* lblError, QObject* parent)
    : QThread(parent)
    , parentt(realParent)
    , hostname("192.168.0.100")
    , port(8081)
    , quit(false)
    , lblErr(lblError) { }

void NetworkThread::run() {
		const int timeout = 3 * 1000;

		QTcpSocket socket;
		socket.connectToHost(hostname, port);
		if ( !socket.waitForConnected(timeout) ) {
				emit error(socket.error(), socket.errorString());
				lblErr->setText("Connection to host failed.");
				return;
		}

		//writing
		char buf[64];
		sprintf(buf, "%s", valueToSend.toStdString().c_str());
		socket.write(buf);
		socket.write("\n");
		lblErr->setText("Attempting to connect.");
		if ( socket.waitForDisconnected(timeout) ) {
				lblErr->setText("Invalid information.");
				return;
		}

		//reading
		lblErr->setText("Logging successful.");
		QString credits;
		do {
				/*if (!socket.waitForReadyRead(timeout)) {
            qDebug() << "nothing more to read";
            break;
        }*/
				credits = socket.readAll();

				qDebug() << credits;
				break;
		} while ( true );

		//true###1000
		QStringList parts = credits.split(QString("###"));
		parentt->totalCreds = parts.at(1).toUInt();

		emit successfulLogin();

		parentt->lock.lock();
		parentt->condition.wait(&parentt->lock);
		parentt->lock.unlock();

		connect(this, &NetworkThread::successfulResult, parentt, &MainWindow::successfulResult);

		while ( true ) {

				qDebug() << "writing the spin: ";
				sprintf(buf, "%s", valueToSend.toStdString().c_str());
				socket.write(buf);
				socket.write("\n");
				qDebug() << "written: " + valueToSend;
				qDebug() << "waiting on response";

				QString response;
				do {
						if ( !socket.waitForReadyRead(timeout) ) {
								qDebug() << "not ready yet";
								break;
						}
						response = socket.readAll();

						qDebug() << response;
				} while ( true );
				emit successfulResult(response);

				parentt->lock.lock();
				parentt->condition.wait(&parentt->lock);
				parentt->lock.unlock();
		}
}
