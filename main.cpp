#include "MainWindow.hpp"

#include <QApplication>
#include <QDebug>
#include <QDesktopWidget>

int main(int argc, char* argv[]) {
		QApplication a(argc, argv);
		MainWindow w;
		w.resolutionWidth = QApplication::desktop()->screenGeometry().width();
		w.resolutionHeight = QApplication::desktop()->screenGeometry().height();
		w.resize(w.resolutionWidth, w.resolutionHeight);
		//w.setWindowState(Qt::WindowFullScreen);
		w.startGame();
		return QApplication::exec();
}
