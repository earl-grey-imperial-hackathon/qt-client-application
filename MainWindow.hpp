#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "NetworkThread.hpp"

#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QMutex>
#include <QPushButton>
#include <QTimer>
#include <QWaitCondition>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
		Q_OBJECT

	    public:
		MainWindow(QWidget* parent = nullptr);
		~MainWindow();

		int resolutionWidth;
		int resolutionHeight;
		void startGame();

		QLabel* lblError;
		QPushButton* exitButton;
		QPushButton* loginButton;
		QPushButton* registerButton;
		QPushButton* spinButton;

		QPushButton* pbReel1;
		int reel1Y = 0;
		QPushButton* pbReel2;
		int reel2Y = 256;
		QPushButton* pbReel3;
		int reel3Y = 512;
		QPushButton* pbReel4;
		int reel4Y = 765;
		QPushButton* pbReel5;
		int reel5Y = 1028;

		QLabel* lblCredits;
		unsigned totalCreds = 0;

		QWaitCondition condition;
		QMutex lock;

		void drawAllFruit();
		void hideAllFruit();
	    public slots:
		void on_pbRegister_clicked();

		void on_pbLogin_clicked();
		void on_pbSpin_clicked();
		void timerTick();
		void successfulLogin();
		void successfulResult(QString res);

		static void exitNow();

	    private:
		Ui::MainWindow* ui;

		void loginScreen();
		void game();

		static QPixmap returnIcon(QString name);

		QGraphicsScene* scene;
		QGraphicsView* view;

		QGraphicsPixmapItem* background_image;
		QGraphicsPixmapItem* title_image = nullptr;
		QGraphicsPixmapItem* table_image;
		QGraphicsPixmapItem* spin_image;

		QLineEdit* leUsername;
		QLineEdit* lePassword;
		QLineEdit* leBet;

		QLabel* lblUsername;
		QLabel* lblPassword;

		QTimer timer;
		int timeElapsed = 0;
		int secondsElapsed = 0;

		QPixmap reel1 = QPixmap(":/symbols/reel.png");

		NetworkThread* thread;
		bool success = false;

		QVector<QVector<QString>> vockice_mi_zivot_upropastile;
};
#endif // MAINWINDOW_HPP
